const cron = require('node-cron');
const dbConfig = require('../config/db.config');

const mysqldump = require('mysqldump')

const fs = require('fs');

let JOB_SCHEDULE =  '0 0 1 * *'

cron.schedule(JOB_SCHEDULE, () => {
  log.info('Run Backup DB every month');
  DumpFileSQL()
});

async function DumpFileSQL(){
    await mysqldump({
        connection: {
            host: dbConfig.HOST,
            user: dbConfig.USER,
            password: dbConfig.PASSWORD,
            database: dbConfig.DB,
        },
        dumpToFile: `./db/${new Date().toLocaleDateString('th')}_dump.sql`,
    });
}
