const express = require('express');
const cors = require('cors');
const app = express();

const dotenv = require('dotenv')
dotenv.config();

require("./db/backup")

const indexRoutes = require("./routes/indexRoutes")

app.use(express.json())
app.use(cors());
app.use(express.urlencoded({extended : true}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get("/", (req, res) => {
    res.json({ message: "Welcome to Backend Application" });
});

app.use("/api",indexRoutes)

app.get("*", (req, res) => {
    res.status(404).send({ message : "404 Not Found"});
});

module.exports = app