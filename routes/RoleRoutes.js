const express = require('express');
const RoleController = require('../controllers/RoleController');


var RoleRouter = express.Router();
var role = new RoleController();

RoleRouter.get("/findAll", role.findAll);
RoleRouter.get("/findByID", role.findByID);
RoleRouter.post("", role.add);
RoleRouter.delete("", role.delete);
RoleRouter.put("",role.update);
RoleRouter.post("/getByCondition",role.getByCondition);
RoleRouter.post("/getByConditionAll",role.getByConditionAll);



module.exports = RoleRouter;
