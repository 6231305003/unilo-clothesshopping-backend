const express = require("express");
const categoryRoutes = require("./CategoryRoutes");
const ColorRoutes = require("./ColorRoutes");
const ProductsRoutes = require("./ProductsRoutes");
const Products_CategoryRoutes = require("./Products_CategoryRoutes");
const Products_ImageRoutes = require("./Products_ImageRoutes");
const Products_StockRoutes = require("./Products_StockRoutes");
const RoleRoutes = require("./RoleRoutes");
const SizeRoutes = require("./SizeRoutes");
const TransactionRoutes = require("./TransactionRoutes");
const UploadsRoutes = require("./UploadsRoutes");
const UsersRoutes = require("./UsersRoutes");

var routes = express.Router();

routes.use("/categories",categoryRoutes);
routes.use("/colors",ColorRoutes);
routes.use("/products",ProductsRoutes);
routes.use("/product_categories",Products_CategoryRoutes);
routes.use("/product_images",Products_ImageRoutes);
routes.use("/product_stocks",Products_StockRoutes);
routes.use("/roles",RoleRoutes);
routes.use("/sizes",SizeRoutes);
routes.use("/transactions",TransactionRoutes);
routes.use("/uploads",UploadsRoutes);
routes.use("/users",UsersRoutes);



module.exports = routes;