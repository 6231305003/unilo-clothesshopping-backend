const express = require('express');
const CategoryController = require('../controllers/CategoryController');


var CategoryRouter = express.Router();
var category = new CategoryController();

CategoryRouter.get("/findAll", category.findAll);
CategoryRouter.get("/findByID", category.findByID);
CategoryRouter.post("", category.add);
CategoryRouter.delete("", category.delete);
CategoryRouter.put("",category.update);
CategoryRouter.post("/getByCondition",category.getByCondition);
CategoryRouter.post("/getByConditionAll",category.getByConditionAll);



module.exports = CategoryRouter;
