const express = require('express');
const Products_CategoryController = require('../controllers/Products_CategoryController');


var Products_CategoryRouter = express.Router();
var product = new Products_CategoryController();

Products_CategoryRouter.get("/findAll", product.findAll);
Products_CategoryRouter.get("/findByID", product.findByID);
Products_CategoryRouter.post("", product.add);
Products_CategoryRouter.delete("", product.delete);
Products_CategoryRouter.put("",product.update);
Products_CategoryRouter.post("/getByCondition",product.getByCondition);
Products_CategoryRouter.post("/getByConditionAll",product.getByConditionAll);



module.exports = Products_CategoryRouter;
