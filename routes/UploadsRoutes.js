const express = require('express');
const UploadsController = require('../controllers/UploadsController');

var multer = require('multer');
const mime = require("mime-types")
const sharp = require("sharp")
var path = require('path');
const filter = (req, file, cb) => {
    if (file.mimetype.split("/")[0] === 'image') {
        cb(null, true);
    } else {
        cb(new Error("Only images are allowed!"));
    }
};

const uploadConfig = multer({ storage: multer.memoryStorage(), limits: { fieldSize: 200 * 1024 * 1024 },fileFilter: filter })

var uploadsRouter = express.Router();
var uploads = new UploadsController();

uploadsRouter.post("/uploadImages",uploadConfig.any("products"),async function (req, res,next) {
    var dataName = []
    for(var img of req.files){
        var nameRN = `${new Date().getTime()}.jpeg`

        const path = `./uploads/products/${nameRN}`;

        await sharp(img.buffer).toFormat("jpeg", { mozjpeg: true }).toFile(path);
        dataName.push(nameRN)
    }
    res.status(200).send({message : "Upload Completed",imageName : dataName})
});

uploadsRouter.get('/products/*', function (req, res) {
    console.log("start get export file :"+req.url);
    var defpath = path.join(__dirname, '../uploads/'+req.url);
    res.sendFile(defpath);
});


module.exports = uploadsRouter;
