const express = require('express');
const ColorController = require('../controllers/ColorController');


var ColorRouter = express.Router();
var color = new ColorController();

ColorRouter.get("/findAll", color.findAll);
ColorRouter.get("/findByID", color.findByID);
ColorRouter.post("", color.add);
ColorRouter.delete("", color.delete);
ColorRouter.put("",color.update);
ColorRouter.post("/getByCondition",color.getByCondition);
ColorRouter.post("/getByConditionAll",color.getByConditionAll);



module.exports = ColorRouter;
