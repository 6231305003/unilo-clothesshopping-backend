const express = require('express');
const Products_ImageController = require('../controllers/Products_ImageController');


var Products_ImageRouter = express.Router();
var product = new Products_ImageController();

Products_ImageRouter.get("/findAll", product.findAll);
Products_ImageRouter.get("/findByID", product.findByID);
Products_ImageRouter.post("", product.add);
Products_ImageRouter.delete("", product.delete);
Products_ImageRouter.put("",product.update);
Products_ImageRouter.post("/getByCondition",product.getByCondition);
Products_ImageRouter.post("/getByConditionAll",product.getByConditionAll);



module.exports = Products_ImageRouter;
