const express = require('express');
const Products_StockController = require('../controllers/Products_StockController');


var Products_StockRouter = express.Router();
var product = new Products_StockController();

Products_StockRouter.get("/findAll", product.findAll);
Products_StockRouter.get("/findByID", product.findByID);
Products_StockRouter.post("", product.add);
Products_StockRouter.delete("", product.delete);
Products_StockRouter.put("",product.update);
Products_StockRouter.post("/getByCondition",product.getByCondition);
Products_StockRouter.post("/getByConditionAll",product.getByConditionAll);



module.exports = Products_StockRouter;
