const express = require('express');
const TransactionController = require('../controllers/TransactionController');


var TransactionRouter = express.Router();
var transaction = new TransactionController();

TransactionRouter.get("/findAll", transaction.findAll);
TransactionRouter.get("/findByID", transaction.findByID);
TransactionRouter.post("", transaction.add);
TransactionRouter.delete("", transaction.delete);
TransactionRouter.put("",transaction.update);
TransactionRouter.post("/getByCondition",transaction.getByCondition);
TransactionRouter.post("/getByConditionAll",transaction.getByConditionAll);



module.exports = TransactionRouter;
