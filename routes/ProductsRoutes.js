const express = require('express');
const ProductsController = require('../controllers/ProductsController');


var ProductsRouter = express.Router();
var product = new ProductsController();

ProductsRouter.get("/findAll", product.findAll);
ProductsRouter.get("/findByID", product.findByID);
ProductsRouter.post("", product.add);
ProductsRouter.delete("", product.delete);
ProductsRouter.put("",product.update);
ProductsRouter.post("/getByCondition",product.getByCondition);
ProductsRouter.post("/getByConditionAll",product.getByConditionAll);



module.exports = ProductsRouter;
