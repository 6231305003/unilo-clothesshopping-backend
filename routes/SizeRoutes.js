const express = require('express');
const SizeController = require('../controllers/SizeController');


var SizeRouter = express.Router();
var size = new SizeController();

SizeRouter.get("/findAll", size.findAll);
SizeRouter.get("/findByID", size.findByID);
SizeRouter.post("", size.add);
SizeRouter.delete("", size.delete);
SizeRouter.put("",size.update);
SizeRouter.post("/getByCondition",size.getByCondition);
SizeRouter.post("/getByConditionAll",size.getByConditionAll);



module.exports = SizeRouter;
