const express = require('express');
const usersController = require('../controllers/UsersController');
const { authenticateToken, password_hashPost } = require('../security')

var usersRouter = express.Router();
var users = new usersController();

usersRouter.post("/login", users.login);
usersRouter.get("/getLogin", authenticateToken, (req, res) => {
    let user = req.user
    console.log(`Action get users : ${user.data.username}`);
    res.json(user)
});
usersRouter.get("/findAll", users.findAll);
usersRouter.get("/findByID", users.findByID);
usersRouter.post("/getPassword", password_hashPost);
usersRouter.get("/findByEmail", users.findByEmail);
usersRouter.get("/findByUsername", users.findByEmail);
usersRouter.post("", users.add);
usersRouter.delete("", users.delete);
usersRouter.put("", users.update);
usersRouter.post("/getByCondition", users.getByCondition);
usersRouter.post("/getByConditionAll", users.getByConditionAll);

module.exports = usersRouter;
