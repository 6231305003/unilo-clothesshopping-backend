const sql = require("./db.js");

// constructor
const Products_Stock = function (obj) {
    this.productStockID = obj.productStockID;
    this.productID = obj.productID;
    this.sizeID = obj.sizeID;
    this.colorID = obj.colorID;
    this.sale = obj.sale;
    this.salePercent = obj.salePercent;
    this.amount = obj.amount;
};

// find all
Products_Stock.getAll = result => {
    sql.query("SELECT Products_Stock.*,Products.*,Color.name as Color,Size.name as Size FROM Products_Stock LEFT JOIN Products ON Products.productID = Products_Stock.productID LEFT JOIN Size ON Size.sizeID = Products_Stock.sizeID LEFT JOIN Color ON Color.colorID = Products_Stock.colorID ORDER BY Products_Stock.productStockID ASC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
};

// find by id
Products_Stock.findById = (id, result) => {
    sql.query("SELECT Products_Stock.*,Products.*,Color.name as Color,Size.name as Size FROM Products_Stock LEFT JOIN Products ON Products.productID = Products_Stock.productID LEFT JOIN Size ON Size.sizeID = Products_Stock.sizeID LEFT JOIN Color ON Color.colorID = Products_Stock.colorID WHERE productStockID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    });
};

// create
Products_Stock.create = (model, result) => {
    sql.query("INSERT INTO Products_Stock SET ?", model, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId });
    });
};

// delete
Products_Stock.remove = (id, result) => {
    sql.query("DELETE FROM Products_Stock WHERE productStockID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err,null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    });
};

// update
Products_Stock.updateById = (bodyData, result) => {
    var uSql = "UPDATE Products_Stock SET ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        if (updateKey == "productStockID") {
            id = updateValue;
            continue;
        }
        uSql += updateKey + " = '" + updateValue + "' , ";
    }
    uSql = uSql.substring(0,uSql.lastIndexOf(","));
    uSql += " WHERE id = ?";
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    }
    );
};
Products_Stock.getByCondition = (bodyData, result) => {
    var uSql = "SELECT Products_Stock.*,Products.*,Color.name as Color,Size.name as Size FROM Products_Stock LEFT JOIN Products ON Products.productID = Products_Stock.productID LEFT JOIN Size ON Size.sizeID = Products_Stock.sizeID LEFT JOIN Color ON Color.colorID = Products_Stock.colorID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Products_Stock.productStockID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    }
    );      
};

Products_Stock.getByConditionAll = (bodyData, result) => {
    var uSql = "SELECT Products_Stock.*,Products.*,Color.name as Color,Size.name as Size FROM Products_Stock LEFT JOIN Products ON Products.productID = Products_Stock.productID LEFT JOIN Size ON Size.sizeID = Products_Stock.sizeID LEFT JOIN Color ON Color.colorID = Products_Stock.colorID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Products_Stock.productStockID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({ type: "not_found" }, null);
        }
    );      
};

module.exports = Products_Stock;