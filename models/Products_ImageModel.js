const sql = require("./db.js");

// constructor
const Products_Image = function (obj) {
    this.productsImageID = obj.productsImageID;
    this.image = obj.image;
    this.date = obj.date;
    this.productID = obj.productID;
};

// find all
Products_Image.getAll = result => {
    sql.query("SELECT * FROM Products_Image LEFT JOIN Products ON Products.productID = Products_Image.productID ORDER BY Products_Image.productsImageID ASC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
};

// find by id
Products_Image.findById = (id, result) => {
    sql.query("SELECT * FROM Products_Image LEFT JOIN Products ON Products.productID = Products_Image.productID WHERE productsImageID = ? ORDER BY Products_Image.productsImageID ASC", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    });
};

// create
Products_Image.create = (model, result) => {
    sql.query("INSERT INTO Products_Image SET ?", model, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId });
    });
};

// delete
Products_Image.remove = (id, result) => {
    sql.query("DELETE FROM Products_Image WHERE productsImageID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err,null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    });
};

// update
Products_Image.updateById = (bodyData, result) => {
    var uSql = "UPDATE Products_Image SET ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        if (updateKey == "productsImageID") {
            id = updateValue;
            continue;
        }
        uSql += updateKey + " = '" + updateValue + "' , ";
    }
    uSql = uSql.substring(0,uSql.lastIndexOf(","));
    uSql += " WHERE id = ?";
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    }
    );
};
Products_Image.getByCondition = (bodyData, result) => {
    var uSql = "SELECT * FROM Products_Image LEFT JOIN Products ON Products.productID = Products_Image.productID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Products_Image.productsImageID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    }
    );      
};

Products_Image.getByConditionAll = (bodyData, result) => {
    var uSql = "SELECT * FROM Products_Image LEFT JOIN Products ON Products.productID = Products_Image.productID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Products_Image.productsImageID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({ type: "not_found" }, null);
        }
    );      
};

module.exports = Products_Image;