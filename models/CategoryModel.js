const sql = require("./db.js");

// constructor
const category = function (obj) {
    this.categoryID = obj.categoryID;
    this.name = obj.name;
};

// find all
category.getAll = result => {
    sql.query("SELECT * FROM Category ORDER BY categoryID ASC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
};

// find by id
category.findById = (id, result) => {
    sql.query("SELECT * FROM Category WHERE categoryID = ? ORDER BY categoryID ASC", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    });
};

// create
category.create = (model, result) => {
    sql.query("INSERT INTO Category SET ?", model, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId });
    });
};

// delete
category.remove = (id, result) => {
    sql.query("DELETE FROM Category WHERE categoryID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err,null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    });
};

// update
category.updateById = (bodyData, result) => {
    var uSql = "UPDATE Category SET ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        if (updateKey == "categoryID") {
            id = updateValue;
            continue;
        }
        uSql += updateKey + " = '" + updateValue + "' , ";
    }
    uSql = uSql.substring(0,uSql.lastIndexOf(","));
    uSql += " WHERE id = ?";
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    }
    );
};
category.getByCondition = (bodyData, result) => {
    var uSql = "SELECT * FROM Category WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY categoryID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    }
    );      
};

category.getByConditionAll = (bodyData, result) => {
    var uSql = "SELECT * FROM Category WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY categoryID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({ type: "not_found" }, null);
        }
    );      
};

module.exports = category;