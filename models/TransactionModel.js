const sql = require("./db.js");

// constructor
const Transaction = function (obj) {
    this.transactionID = obj.transactionID;
    this.transactionSerial = obj.transactionSerial;
    this.userID = obj.userID;
    this.productID = obj.productID;
    this.colorID = obj.colorID;
    this.sizeID = obj.sizeID;
    this.amount = obj.amount;
};

// find all
Transaction.getAll = result => {
    sql.query("SELECT Transaction.*,Users.fullname,Size.name as Size,Color.name as Color FROM Transaction LEFT JOIN Users ON Users.userID = Transaction.userID LEFT JOIN Size ON Size.sizeID = Transaction.sizeID LEFT JOIN Color ON Color.colorID = Transaction.colorID LEFT JOIN Products ON Products.productID = Transaction.productID ORDER BY Transaction.transactionID ASC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
};

// find by id
Transaction.findById = (id, result) => {
    sql.query("SELECT Transaction.*,Users.fullname,Size.name as Size,Color.name as Color FROM Transaction LEFT JOIN Users ON Users.userID = Transaction.userID LEFT JOIN Size ON Size.sizeID = Transaction.sizeID LEFT JOIN Color ON Color.colorID = Transaction.colorID LEFT JOIN Products ON Products.productID = Transaction.productID WHERE transactionID = ? ORDER BY Transaction.transactionID ASC", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    });
};

// create
Transaction.create = (model, result) => {
    sql.query("INSERT INTO Transaction SET ?", model, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId });
    });
};

// delete
Transaction.remove = (id, result) => {
    sql.query("DELETE FROM Transaction WHERE transactionID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err,null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    });
};

// update
Transaction.updateById = (bodyData, result) => {
    var uSql = "UPDATE Transaction SET ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        if (updateKey == "transactionID") {
            id = updateValue;
            continue;
        }
        uSql += updateKey + " = '" + updateValue + "' , ";
    }
    uSql = uSql.substring(0,uSql.lastIndexOf(","));
    uSql += " WHERE id = ?";
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    }
    );
};
Transaction.getByCondition = (bodyData, result) => {
    var uSql = "SELECT Transaction.*,Users.fullname,Size.name as Size,Color.name as Color FROM Transaction LEFT JOIN Users ON Users.userID = Transaction.userID LEFT JOIN Size ON Size.sizeID = Transaction.sizeID LEFT JOIN Color ON Color.colorID = Transaction.colorID LEFT JOIN Products ON Products.productID = Transaction.productID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Transaction.transactionID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    }
    );      
};

Transaction.getByConditionAll = (bodyData, result) => {
    var uSql = "SELECT Transaction.*,Users.fullname,Size.name as Size,Color.name as Color FROM Transaction LEFT JOIN Users ON Users.userID = Transaction.userID LEFT JOIN Size ON Size.sizeID = Transaction.sizeID LEFT JOIN Color ON Color.colorID = Transaction.colorID LEFT JOIN Products ON Products.productID = Transaction.productID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Transaction.transactionID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({ type: "not_found" }, null);
        }
    );      
};

module.exports = Transaction;