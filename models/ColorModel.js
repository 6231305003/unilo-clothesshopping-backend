const sql = require("./db.js");

// constructor
const Color = function (obj) {
    this.colorID = obj.colorID;
    this.name = obj.name;
};

// find all
Color.getAll = result => {
    sql.query("SELECT * FROM Color ORDER BY colorID ASC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
};

// find by id
Color.findById = (id, result) => {
    sql.query("SELECT * FROM Color WHERE colorID = ? ORDER BY colorID ASC", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    });
};

// create
Color.create = (model, result) => {
    sql.query("INSERT INTO Color SET ?", model, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId });
    });
};

// delete
Color.remove = (id, result) => {
    sql.query("DELETE FROM Color WHERE colorID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err,null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    });
};

// update
Color.updateById = (bodyData, result) => {
    var uSql = "UPDATE Color SET ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        if (updateKey == "colorID") {
            id = updateValue;
            continue;
        }
        uSql += updateKey + " = '" + updateValue + "' , ";
    }
    uSql = uSql.substring(0,uSql.lastIndexOf(","));
    uSql += " WHERE id = ?";
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    }
    );
};
Color.getByCondition = (bodyData, result) => {
    var uSql = "SELECT * FROM Color WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY colorID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    }
    );      
};

Color.getByConditionAll = (bodyData, result) => {
    var uSql = "SELECT * FROM Color WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY colorID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({ type: "not_found" }, null);
        }
    );      
};

module.exports = Color;