const sql = require("./db.js");

// constructor
const Products_Category = function (obj) {
    this.productCatID = obj.productCatID;
    this.productID = obj.productID;
    this.categoryID = obj.categoryID;
};

// find all
Products_Category.getAll = result => {
    sql.query("SELECT Products_Category.productCatID,Category.*,Products.* FROM Products_Category LEFT JOIN Category ON Category.categoryID = Products_Category.categoryID LEFT JOIN Products ON Products.productID = Products_Category.productID ORDER BY Products_Category.productCatID ASC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
};

// find by id
Products_Category.findById = (id, result) => {
    sql.query("SELECT Products_Category.productCatID,Category.*,Products.* FROM Products_Category LEFT JOIN Category ON Category.categoryID = Products_Category.categoryID LEFT JOIN Products ON Products.productID = Products_Category.productID WHERE productCatID = ? ORDER BY Products_Category.productCatID ASC", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    });
};

// create
Products_Category.create = (model, result) => {
    sql.query("INSERT INTO Products_Category SET ?", model, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId });
    });
};

// delete
Products_Category.remove = (id, result) => {
    sql.query("DELETE FROM Products_Category WHERE productCatID = ?", id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err,null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    });
};

// update
Products_Category.updateById = (bodyData, result) => {
    var uSql = "UPDATE Products_Category SET ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        if (updateKey == "productCatID") {
            id = updateValue;
            continue;
        }
        uSql += updateKey + " = '" + updateValue + "' , ";
    }
    uSql = uSql.substring(0,uSql.lastIndexOf(","));
    uSql += " WHERE id = ?";
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            // not found with the id
            result({ type: "not_found" }, null);
            return;
        }
        result(null, { id: id });
    }
    );
};
Products_Category.getByCondition = (bodyData, result) => {
    var uSql = "SELECT Products_Category.productCatID,Category.*,Products.* FROM Products_Category LEFT JOIN Category ON Category.categoryID = Products_Category.categoryID LEFT JOIN Products ON Products.productID = Products_Category.productID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Products_Category.productCatID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({ type: "not_found" }, null);
    }
    );      
};

Products_Category.getByConditionAll = (bodyData, result) => {
    var uSql = "SELECT Products_Category.productCatID,Category.*,Products.* FROM Products_Category LEFT JOIN Category ON Category.categoryID = Products_Category.categoryID LEFT JOIN Products ON Products.productID = Products_Category.productID WHERE 1 = 1 ";
    var id = null;
    for (var index in bodyData) {
        var updateKey = bodyData[index].key;
        var updateValue = bodyData[index].value;
        uSql += " AND "+ updateKey + " = '" + updateValue + "' ";
    }
    uSql += " ORDER BY Products_Category.productCatID ASC"
    sql.query(uSql, id , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({ type: "not_found" }, null);
        }
    );      
};

module.exports = Products_Category;