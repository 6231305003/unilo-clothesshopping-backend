/*
 Navicat Premium Data Transfer

 Source Server         : hirodb1
 Source Server Type    : MySQL
 Source Server Version : 80023 (8.0.23-PlanetScale)
 Source Host           : ap-southeast.connect.psdb.cloud:3306
 Source Schema         : hirodb-1

 Target Server Type    : MySQL
 Target Server Version : 80023 (8.0.23-PlanetScale)
 File Encoding         : 65001

 Date: 19/12/2022 11:44:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Category
-- ----------------------------
DROP TABLE IF EXISTS `Category`;
CREATE TABLE `Category`  (
  `categoryID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`categoryID`) USING BTREE,
  UNIQUE INDEX `name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Category
-- ----------------------------
INSERT INTO `Category` VALUES (2, 'Armwear');
INSERT INTO `Category` VALUES (3, 'Children\'s clothing');
INSERT INTO `Category` VALUES (4, 'Dresses');
INSERT INTO `Category` VALUES (5, 'Jackets');
INSERT INTO `Category` VALUES (6, 'Outerwear');
INSERT INTO `Category` VALUES (7, 'Skirts');
INSERT INTO `Category` VALUES (8, 'Sportswear');
INSERT INTO `Category` VALUES (9, 'Suits');
INSERT INTO `Category` VALUES (1, 'Tops');
INSERT INTO `Category` VALUES (10, 'Trousers and shorts');

-- ----------------------------
-- Table structure for Color
-- ----------------------------
DROP TABLE IF EXISTS `Color`;
CREATE TABLE `Color`  (
  `colorID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`colorID`) USING BTREE,
  UNIQUE INDEX `name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Color
-- ----------------------------
INSERT INTO `Color` VALUES (1, 'Black');
INSERT INTO `Color` VALUES (3, 'Blue');
INSERT INTO `Color` VALUES (13, 'Cream');
INSERT INTO `Color` VALUES (10, 'Green');
INSERT INTO `Color` VALUES (9, 'Light Green');
INSERT INTO `Color` VALUES (2, 'Orange');
INSERT INTO `Color` VALUES (7, 'Pink');
INSERT INTO `Color` VALUES (6, 'Purple');
INSERT INTO `Color` VALUES (8, 'Red');
INSERT INTO `Color` VALUES (12, 'Sky Blue');
INSERT INTO `Color` VALUES (5, 'White');
INSERT INTO `Color` VALUES (4, 'Yellow');

-- ----------------------------
-- Table structure for Products
-- ----------------------------
DROP TABLE IF EXISTS `Products`;
CREATE TABLE `Products`  (
  `productID` int NOT NULL AUTO_INCREMENT,
  `productSerial` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `productName` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `productDetail` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `productStatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `productPublish` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`productID`) USING BTREE,
  UNIQUE INDEX `productSerial`(`productSerial` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Products
-- ----------------------------
INSERT INTO `Products` VALUES (1, '456287', 'เสื้อแขนสั้น MAKOTO SHINKAI FILMS COLLECTION UT', 'นำเสนอภาพกราฟิกของฉากที่สวยงามจากภาพยนตร์แอนิเมชัน “Weathering With You\" ด้วยท้องฟ้าอันสดใสที่ทอแสงอย่างงดงาม พร้อมนำประโยคอันแสนตราตรึงจากบทภาพยนตร์มาเรียงร้อยเป็นข้อความเพื่อสื่อถึงอารมณ์ได้อย่างโดดเด่น', 'open', '2022-12-19 01:55:54');
INSERT INTO `Products` VALUES (2, '456288', 'เสื้อแขนสั้น MAKOTO SHINKAI FILMS COLLECTION UT', 'ดาวหางสุกสว่างที่พาดผ่านท้องฟ้ายามค่ำคืนจนเกิดเป็นภาพอันน่าอัศจรรย์จาก \"Your Name\" คือภาพกราฟิกหลักในไอเทมนี้ ผสานดีไซน์แนวสตรีทเพื่อนำเสนอทิวทัศน์อันสวยงามราวกับต้องมนตร์แห่งความฝันอันเป็นเสน่ห์ตามแบบฉบับในผลงานของ Shinkai', 'open', '2022-12-19 01:56:44');
INSERT INTO `Products` VALUES (3, '456286', 'เสื้อแขนสั้น MAKOTO SHINKAI FILMS COLLECTION UT', 'ดีไซน์นี้ได้แรงบันดาลใจจากภาพยนตร์แอนิเมชัน “Suzume\" โดยใช้ภาพประตูอันเดียวดายเป็นองค์ประกอบหลักในการสื่อถึงเรื่องราวอย่างมีพลัง พร้อมด้วยภาพกุญแจซึ่งเป็นจุดเด่นจากภาพยนตร์อีกอย่างหนึ่ง ภาพกุญแจที่ด้านหลังของเสื้อยืดอยู่ในตำแหน่งตรงกลางราวกับถูกตรึงไว้', 'open', '2022-12-19 01:57:09');
INSERT INTO `Products` VALUES (4, '456289', 'เสื้อแขนสั้น MAKOTO SHINKAI FILMS COLLECTION UT', 'Daijin ตัวละครลึกลับที่ทำให้เรื่องราว \"Suzume\" ดูมีชีวิตชีวาถูกนำเสนอเป็นภาพลายปักสุดพิเศษ ส่วนด้านหลังเสื้อเป็นภาพ \"เก้าอี้ของ Suzume” ที่เต็มไปด้วยปริศนา ดีไซน์ของเสื้อยืดเผยให้เห็นถึงความน่ารักของตัวละครทั้งสองที่ต้องเผชิญหน้ากันในภาพยนตร์ได้อย่างโดดเด่น', 'closed', '2022-12-19 01:57:29');
INSERT INTO `Products` VALUES (5, '460291', 'เสื้อแขนสั้น SPY x FAMILY UT', 'บอนด์ สุนัขตัวใหญ่ที่มีความสามารถในการมองเห็นอนาคต ผ่านโปรเจกต์ทดลองสัตว์ \"Apple\" มีเหตุที่ทำให้ได้พบกับอาเนีย ฟอร์เจอร์ ผู้มีพลังจิตอ่านใจคน ด้วยดีไซน์ลายตัวเล็กที่หน้าอกทำให้เป็นเสื้อที่สวมใส่ได้ง่าย', 'open', '2022-12-19 01:58:36');
INSERT INTO `Products` VALUES (6, '460289', 'เสื้อแขนสั้น SPY x FAMILY UT', 'อาเนียผู้มีพลังจิตสามารถอ่านใจคนได้ และบอนด์สุนัขตัวใหญ่ที่มีความสามารถในการมองเห็นอนาคต ดีไซน์ออริจินอลที่รวมสองผู้มีพลังเหนือธรรมชาติ เป็นเสื้อยืดที่ทั้งผู้ใหญ่และเด็กสนุกสนานไปด้วยกันได้', 'sale', '2022-12-19 01:58:54');
INSERT INTO `Products` VALUES (7, '459283', 'เสื้อแขนสั้น SPY x FAMILY UT', 'ดีไซน์ที่คุณเพลิดเพลินไปกับหลากหลายสีหน้าของอาเนีย เช่น ใบหน้าประหลาดใจ ซุกซน กำลังร้องไห้ และอื่นๆ “I LIKE PEANUTS I HATE CAAROTS” ถ่ายทอดออกมาในรูปแบบตัวหนังสือ ถั่วลิสงที่อาเนียชอบก็แอบนำมาพิมพ์อยู่ด้านหลังด้วย เป็นเสื้อยืดที่ผู้ใหญ่และเด็กสนุกสนานไปด้วยกันได้', 'sale', '2022-12-19 01:59:10');
INSERT INTO `Products` VALUES (8, '460290', 'เสื้อแขนสั้น SPY x FAMILY UT', 'ดีไซน์ต้นฉบับมาจากฉากเปิดอนิเมะ ทีวีของบ้านฟอร์เจอร์ที่นำมาสร้างสรรค์ในสไตล์ย้อนยุคสุดน่ารัก', 'closed', '2022-12-19 01:59:31');

-- ----------------------------
-- Table structure for Products_Category
-- ----------------------------
DROP TABLE IF EXISTS `Products_Category`;
CREATE TABLE `Products_Category`  (
  `productCatID` int NOT NULL AUTO_INCREMENT,
  `productID` int NOT NULL,
  `categoryID` int NOT NULL,
  PRIMARY KEY (`productCatID`, `productID`, `categoryID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Products_Category
-- ----------------------------
INSERT INTO `Products_Category` VALUES (1, 1, 1);
INSERT INTO `Products_Category` VALUES (2, 2, 1);
INSERT INTO `Products_Category` VALUES (3, 3, 1);
INSERT INTO `Products_Category` VALUES (4, 4, 1);
INSERT INTO `Products_Category` VALUES (5, 5, 1);
INSERT INTO `Products_Category` VALUES (6, 6, 1);
INSERT INTO `Products_Category` VALUES (7, 7, 1);
INSERT INTO `Products_Category` VALUES (8, 8, 1);

-- ----------------------------
-- Table structure for Products_Image
-- ----------------------------
DROP TABLE IF EXISTS `Products_Image`;
CREATE TABLE `Products_Image`  (
  `productsImageID` int NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `date` datetime NULL DEFAULT NULL,
  `productID` int NULL DEFAULT NULL,
  PRIMARY KEY (`productsImageID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Products_Image
-- ----------------------------
INSERT INTO `Products_Image` VALUES (1, '1671415426127.webp', '2022-12-19 09:32:57', 1);
INSERT INTO `Products_Image` VALUES (2, '1671416333028.webp', '2022-12-19 09:32:57', 1);
INSERT INTO `Products_Image` VALUES (3, '1671416354017.webp', '2022-12-19 09:32:57', 1);
INSERT INTO `Products_Image` VALUES (4, '1671416370384.webp', '2022-12-19 09:34:04', 1);
INSERT INTO `Products_Image` VALUES (5, '1671416392729.webp', '2022-12-19 09:34:04', 1);
INSERT INTO `Products_Image` VALUES (6, '1671416974905.webp', '2022-12-19 09:34:04', 2);
INSERT INTO `Products_Image` VALUES (7, '1671416975148.webp', '2022-12-19 09:34:04', 2);
INSERT INTO `Products_Image` VALUES (8, '1671416975360.webp', '2022-12-19 09:34:04', 2);
INSERT INTO `Products_Image` VALUES (9, '1671416975599.webp', '2022-12-19 09:34:04', 2);
INSERT INTO `Products_Image` VALUES (10, '1671417100034.jpeg', '2022-12-19 09:34:04', 3);
INSERT INTO `Products_Image` VALUES (11, '1671417100291.jpeg', '2022-12-19 09:34:04', 3);
INSERT INTO `Products_Image` VALUES (12, '1671417100507.jpeg', '2022-12-19 09:34:04', 3);
INSERT INTO `Products_Image` VALUES (13, '1671417100773.jpeg', '2022-12-19 09:34:04', 3);
INSERT INTO `Products_Image` VALUES (14, '1671417107175.jpeg', '2022-12-19 09:34:04', 3);

-- ----------------------------
-- Table structure for Products_Stock
-- ----------------------------
DROP TABLE IF EXISTS `Products_Stock`;
CREATE TABLE `Products_Stock`  (
  `productStockID` int NOT NULL AUTO_INCREMENT,
  `productID` int NOT NULL,
  `sizeID` int NOT NULL,
  `colorID` int NOT NULL,
  `sale` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `salePercent` int NOT NULL,
  `amount` int NULL DEFAULT NULL,
  PRIMARY KEY (`productStockID`, `productID`, `sizeID`, `colorID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Products_Stock
-- ----------------------------
INSERT INTO `Products_Stock` VALUES (1, 1, 3, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (2, 1, 4, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (3, 1, 5, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (4, 1, 6, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (5, 2, 3, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (6, 2, 4, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (7, 2, 5, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (8, 2, 6, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (9, 3, 3, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (10, 3, 4, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (11, 3, 5, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (12, 3, 6, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (13, 4, 3, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (14, 4, 4, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (15, 4, 5, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (16, 4, 6, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (17, 5, 3, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (18, 5, 4, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (19, 5, 5, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (20, 5, 6, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (21, 6, 3, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (22, 6, 4, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (23, 6, 5, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (24, 6, 6, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (25, 7, 3, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (26, 7, 4, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (27, 7, 5, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (28, 7, 6, 1, 'open', 20, 100);
INSERT INTO `Products_Stock` VALUES (29, 8, 3, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (30, 8, 4, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (31, 8, 5, 1, 'closed', 0, 100);
INSERT INTO `Products_Stock` VALUES (32, 8, 6, 1, 'closed', 0, 100);

-- ----------------------------
-- Table structure for Role
-- ----------------------------
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role`  (
  `roleID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`roleID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Role
-- ----------------------------
INSERT INTO `Role` VALUES (1, 'admin');
INSERT INTO `Role` VALUES (2, 'users');

-- ----------------------------
-- Table structure for Size
-- ----------------------------
DROP TABLE IF EXISTS `Size`;
CREATE TABLE `Size`  (
  `sizeID` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sizeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Size
-- ----------------------------
INSERT INTO `Size` VALUES (1, 'XXS');
INSERT INTO `Size` VALUES (2, 'XS');
INSERT INTO `Size` VALUES (3, 'S');
INSERT INTO `Size` VALUES (4, 'M');
INSERT INTO `Size` VALUES (5, 'L');
INSERT INTO `Size` VALUES (6, 'XL');
INSERT INTO `Size` VALUES (7, 'XXL');
INSERT INTO `Size` VALUES (8, 'XXXL');
INSERT INTO `Size` VALUES (9, 'Oversize');

-- ----------------------------
-- Table structure for Transaction
-- ----------------------------
DROP TABLE IF EXISTS `Transaction`;
CREATE TABLE `Transaction`  (
  `transactionID` int NOT NULL AUTO_INCREMENT,
  `transactionSerial` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `userID` int NOT NULL,
  `productID` int NOT NULL,
  `colorID` int NOT NULL,
  `sizeID` int NOT NULL,
  `amount` int NULL DEFAULT NULL,
  PRIMARY KEY (`transactionID`, `userID`, `productID`, `colorID`, `sizeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Transaction
-- ----------------------------
INSERT INTO `Transaction` VALUES (1, 'ax1234', 2, 1, 1, 3, 1);
INSERT INTO `Transaction` VALUES (4, 'ax1234', 2, 1, 1, 4, 1);
INSERT INTO `Transaction` VALUES (5, 'ax1234', 2, 1, 1, 5, 1);

-- ----------------------------
-- Table structure for Users
-- ----------------------------
DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users`  (
  `userID` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `profile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `roleID` int NULL DEFAULT NULL,
  PRIMARY KEY (`userID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Users
-- ----------------------------
INSERT INTO `Users` VALUES (1, 'hirodesu', 'hr@mail.com', 'a1953eb0a0d211b635d632fa468e38f93429c79b923255d1dfd8e1243e3bf2b0', 'default.png', 'HiRO Desu', '0910000000', 1);
INSERT INTO `Users` VALUES (2, 'hirosama', 'hs@mail.com', '013c22a0bc8332bb7726640fc29f00586c0304728560e5b4e52871538094a9a1', 'default.png', 'HiRO sama', '0910000000', 2);

SET FOREIGN_KEY_CHECKS = 1;
