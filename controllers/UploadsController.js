const mime = require("mime-types")
const sharp = require("sharp")


class UploadsController {
  uploadImages(req, res){
    return new Promise(async (resolve, reject,next) => {
        await sleepTime(1000)

        var extFile = mime.extension(req.file.mimetype)
        var nameRN = `${new Date().getTime()}.${extFile}`

        const path = `./uploads/products/${nameRN}`;

        await sharp(req.file.buffer).jpeg({ mozjpeg: true }).toFile(path);

        req.imageName = nameRN

        next();
    });
  }

}

async function sleepTime(duration, ...args){
  await new Promise(resolve => setTimeout(resolve, duration));
}

module.exports = UploadsController;
